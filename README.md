# Примеры кода для курса ПК Цифровые компетенции: основы программной реализации алгоритмов (Программист)

В этом репозитории приведены примеры кода, которые демонстрировались на лекциях и в практических примерах в онлайн-курсе [ПК Цифровые компетенции: основы программной реализации алгоритмов (Программист)](https://demidonline.uniyar.ac.ru/courses/course-v1:DemidOnline+Prog002.01x+2021/courseware?activate_block_id=block-v1%3ADemidOnline%2BProg002.01x%2B2021%2Btype%40course%2Bblock%40course).

Обратите внимание, что здесь приведён только код программ, а не полные проекты для сред разработки.

Также можно изучить примеры проектов [на Java для IntelliJ Idea](https://gitlab.com/lagutinakv/java-programming-example/) и [на Python для PyCharm](https://gitlab.com/lagutinakv/programming-example).
