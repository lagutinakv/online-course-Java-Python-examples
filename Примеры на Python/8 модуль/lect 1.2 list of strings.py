print('Введите слова в одну строку через пробел:')
words = input().split()
print('Список слов: ', words)
while True:
    print('Введите i, чтобы добавить слово, r, чтобы удалить слово, q, чтобы выйти из программы')
    command = input()
    if command == 'q':
        break
    elif command == 'i':
        word = input('Введите новое слово: ')
        words.append(word)
    elif command == 'r':
        word = input('Введите слово, которое хотите удалить: ')
        if word in words:
            words.remove(word)
        else:
            print('Такого слова нет')
    print('Слова в списке: ', ' '.join(words))
