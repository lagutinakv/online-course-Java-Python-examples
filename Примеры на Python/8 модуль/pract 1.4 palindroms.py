# Считайте строку, разбейте её на слова по пробелам.
# Подсчитайте, сколько слов являются палиндромами.
from functools import reduce

words = input('Введите слова через пробел:').split() # Считываем строку и разбиваем на слова по пробелам
counter = 0 # Счётчик палиндромов
for word in words: # Перебираем все слова
    if word == word[::-1]: # Если слово совпадает со своей перевёрнутой версией, это палиндром
        counter += 1
print(counter)

# Альтернативно: то же самое в функциональном стиле
counter = reduce(lambda res, word: res + 1 if word == word[::-1] else res, words, 0)
print(counter)
