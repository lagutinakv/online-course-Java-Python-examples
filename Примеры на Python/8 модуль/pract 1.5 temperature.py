# Задан дневник температуры за март:
# {-2, -5, -2, -4, 3, -6, -2, -1, 5, 1, 1, 0, -1, 0, 3, -1, 2, 5, 2, 4, 4, 0, 6, 1, 4, 6, -1, 2, 4, 7, 11}.
# Найти:
# количество дней с отрицательной температурой,
# максимальную температуру в первую неделю марта.
from functools import reduce

# Задаём дневник как массив
marth_temperatures = [-2, -5, -2, -4, 3, -6, -2, -1, 5, 1, 1, 0, -1, 0, 3, -1, 2, 5, 2, 4, 4, 0, 6, 1, 4, 6, -1, 2, 4, 7, 11]
count_days = 0
for day in marth_temperatures: # Перебираем все дни, считаем отрицательные числа
    if day < 0:
        count_days += 1
print(count_days)
# Альтернативно: то же самое в функциональном стиле
count_days = reduce(lambda res, day: res + 1 if day < 0 else res, marth_temperatures, 0)
print(count_days)

max_temp = max(marth_temperatures[:7]) # Берём первую неделю марта как срез массива и ищем в срезе максимум
print(max_temp)

# Альтернативно: ищем максимум первой недели с помощью цикла
max_temp = -1000
for i in range(7): # Перебираем только первые 7 дней
    max_temp = max(max_temp, marth_temperatures[i]) # Берём максимум из найденного ранее максимума и текущего элемента
print(max_temp)
