# Заполнить массив 20 случайными целыми числами (диапазон чисел от 0 до 1000).
# Сформировать новый массив, переписав в него те элементы, которые находятся в диапазоне,
# заданном пользователем
import random
nums = [] # Пустой массив
# Заполняем массив 20 случайными числами
for i in range(20):
    nums.append(random.randint(0, 1000))
# Выводим получившийся массив
print(nums)
# Задаём диапазон
min_range = int(input('Задайте минимум диапазона: '))
max_range = int(input('Задайте максимум диапазона: '))
new_nums = [] # Новый пустой массив
for num in nums: # Перебираем все элементы и добавляем в новый массив те, которые входят в диапазон
    if min_range <= num <= max_range:
        new_nums.append(num)
print(new_nums)
# Альтернативно: тот же перебор в функциональном стиле
filtered_nums = filter(lambda num: min_range <= num <= max_range, nums)
print(list(filtered_nums))
