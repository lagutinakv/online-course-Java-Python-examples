# Задача на работу с документацией модуля datetime.
# Создайте дату 01.09.2021 и время 12.30.
# Соедините их в общую datetime с помощью метода combine, который изучите в документации.
# Найдите текущую дату (метод now у datetime)
# Найдите разность между текущей датой и 01.09.2021 12.30

from datetime import datetime, date, time # Импортируем три типа данных из модуля datetime

# Создаём отдельно дату и время
old_date = date(2021, 9, 1)
old_time = time(12, 30)
old_datetime = datetime.combine(old_date, old_time) # Пример комбинации даты и времени в общий тип datetime

current_datetime = datetime.now() # Получаем текущую дату
difference = current_datetime - old_datetime # Разность между двумя датами
print(difference)
print(type(difference)) # Тип разности - timedelta
