print('Введите цену шоколадки')
price = float(input())
print('Введите количество шоколадок')
amount = int(input())
cost = price * amount
print('Вы покупаете %d шоколадок за %f рублей'% (amount, cost))
print('Вы покупаете %d шоколадок за %.2f рублей'% (amount, cost))
print(f'Вы покупаете {amount} шоколадок за {cost} рублей')
print(f'Вы покупаете {amount} шоколадок за {cost:.2f} рублей')
