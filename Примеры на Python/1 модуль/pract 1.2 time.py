# Переменная для ускорения свободного падения
g = 9.81
print('Введите высоту как дробное число:')
h = float(input()) # Преобразуем строку в число с плавающей точкой, получаем метры
t = (2*h/g)**0.5 # Вычисление времени. Чтобы сделать возведение в степень последним действием, ставим скобки
print(f'Время падения с высоты {h} метров - {t} секунд') # Форматированный вывод значений h и t
