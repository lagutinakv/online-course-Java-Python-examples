# Заводим переменные для всех данных
tea_consumption = 80
days = 4
tea_pack_size = 100
# Умножаем число чашек чая в день на количество дней и получаем, сколько нужно пакетиков на всю конференцию
# Делим полученное число пакетиков на размер упаковки нацело и добавляем 1, чтобы купить пачек с запасом
# Операции будут выполняться слева направо, + менее приоритетная, чем умножение и деление
tea_pack_num = tea_consumption * days // tea_pack_size + 1
print(tea_pack_num)
