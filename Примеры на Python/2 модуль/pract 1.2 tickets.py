# Ввод исходных данных
n = int(input('Введите N:'))
m = int(input('Введите M:'))
# Проверка, корректные ли данные введены
if n < 100000 or n > 999999 or m < 100000 or m > 999999:
    print('N и M должны быть шестизначными')
    exit()
if n > m:
    print('N не может быть больше M')
    exit()
# Заводим счётчик счастливых билетов
counter = 0
for num in range(n, m + 1): # Перебираем числа от n до m включительно
    # Суммируем последние 3 цифры
    last_digits_sum = num % 10 + num // 10 % 10 + num // 100 % 10
    # Отбрасываем последние 3 цифры, получаем трёхзначное число
    first_digits = num // 1000
    # Суммируем 3 цифры полученного числа
    first_digits_sum = first_digits % 10 + first_digits // 10 % 10 + first_digits // 100 % 10
    if last_digits_sum == first_digits_sum:
        # Если билет счастливый, увеличиваем счётчик
        counter += 1
print(counter)
