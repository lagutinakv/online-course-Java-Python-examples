month = int(input()) # Вводим номер месяца
if month > 12 or month < 1: # Если номер месяца не корректен
    print('Номер месяца должен быть числом от 1 до 12')
    exit()
if month == 1 or month == 3 or month == 5 or month == 7 or \
    month == 8 or month == 10 or month == 12:
    # Если месяц - январь, март, май, июль, август, октябрь или декабрь
    print('31')
elif month == 2: # Если месяц - февраль
    print('28 или 29 в високосный год')
else: # Остальные месяцы
    print('30')
