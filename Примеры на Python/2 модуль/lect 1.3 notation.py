# Найти сумму цифр числа в десятичной и в заданной пользователем системе счисления

# notation = int(input('Введите систему счисления:'))
notation = 10
number = int(input('Введите число:'))
digit_sum = 0
while number > 0:
    digit = number % notation
    digit_sum += digit
    number //= notation
print(digit_sum)
