import pytest
from triangle import isTriangle, area, radius


@pytest.mark.parametrize("a, b, c", [(3,4,5), (1,1,1),(3,4,6)])
def test_is_triangle(a, b, c):
    assert isTriangle(a, b, c)


@pytest.mark.parametrize("third_side", [2, 3, 12])
def test_not_triangle(third_side):
    assert not isTriangle(1, 1, 3)


@pytest.mark.parametrize("a, b, c, triangle_area",
                         [(3,4,5,6), (1,1,1,0.4330127018922193),(3,4,6,5.332682251925386)])
def test_area(a, b, c, triangle_area):
    assert area(a, b, c) == triangle_area


@pytest.mark.parametrize("a, b, c, triangle_radius",
                         [(3,4,5,2.5), (1,1,1,0.5773502691896258), (3,4,6,3.375412062757167)])
def test_radius(a, b, c, triangle_radius):
    assert radius(a, b, c) == triangle_radius
